All participants in Guild of Maker events are required to agree with the 
following code of conduct to help achieve a positive experience for everyone. 
It is expected participants to conduct themselves appropriately.

* No intimidating, harassing, abusive, discriminatory, offensive language, 
derogatory or demeaning conduct at events or online.
* Respect alternative opinions
* Allow dialogue to occur
* Show courtesy to other speakers and members

If a participant engages in behaviour that violates this code of conduct, 
the event organiser will take the appropriate action, including warning or 
expulsion. These actions will be discussed with Guild of Makers Founders to see 
if further items need to be discussed. 
